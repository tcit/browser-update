export enum LinuxDistributions {
  LINUX = "Linux",
  FEDORA = "Fedora",
  UBUNTU = "Ubuntu",
  DEBIAN = "Debian",
  ARCH_LINUX = "ArchLinux",
}

export enum BSD {
  FREEBSD = "FreeBSD",
}

export enum RenderingEngines {
  GECKO = "Gecko",
}

export enum Android {
  ANDROID = "Android",
}

export enum iOS {
  IOS = "iOS",
  IPADOS = "iPadOS",
}

export const WINDOWS = "Windows";

export const MACOS = "macOS";

export enum MACOS_VERSIONS {
  SIERRA = "Sierra",
  HIGH_SIERRA = "HighSierra",
  CATALINA = "Catalina",
  MOJAVE = "Mojave",
  BIGSUR = "BigSur",
}
