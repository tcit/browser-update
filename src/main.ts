import { createApp } from "vue";
import { createGettext } from "vue3-gettext";
import App from "./App.vue";
import translations from "./locale/translations.json";
import "./index.css";

const app = createApp(App);

const gettext = createGettext({
  availableLanguages: {
    en_GB: "British English",
    fr_FR: "Français",
    en_US: "American English",
  },
  defaultLanguage: "fr_FR",
  translations,
});

app.use(gettext);

app.mount("#app");
