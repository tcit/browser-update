import { matchesUA } from "browserslist-useragent";
import { agents } from "caniuse-lite";
import bowser from "bowser";
import { compareVersions } from "compare-versions";

function getLatestVersion(browser: string|undefined): number {
  console.log("latestversion agents", agents);
  if (browser === undefined) return 0;
  const lowerCaseBrowser = browser.toLowerCase();
  if (Object.prototype.hasOwnProperty.call(agents, lowerCaseBrowser)) {
    return Math.max(
      ...Object.entries(agents[lowerCaseBrowser]?.release_date ?? {})
        .filter(([release, date]) => date != null)
        .map(([release, _]) => parseFloat(release))
    );
  }
  return 0;
}

export default function (userAgent: string) {
  const browser: {
    uptodate: boolean,
    unreleased: boolean,
    esr: boolean,
    dead: boolean,
    maybe_up_to_date: boolean,
    matchesLastVersion?: boolean,
    matchesLastTwoVersions?: boolean,
    bowser: bowser.Parser.ParsedResult | undefined,
    latest?: number
    version?: string
    downloadURL?: string | undefined | null
    name?: string,
    real_name?: string,
    internal_name?: string
    simpleVersion?: string
    os?: string
  } = {
    uptodate: false,
    unreleased: false,
    esr: false,
    dead: false,
    maybe_up_to_date: false,
    bowser: undefined,
    latest: undefined
  };

  const result = bowser.parse(userAgent);
  browser.bowser = result;

  if (
    matchesUA(userAgent, {
      browsers: ["last 1 versions"],
      path: ".",
      allowHigherVersions: true,
    })
  ) {
    browser.uptodate = true;
    browser.matchesLastVersion = true;
  }

  if (
    matchesUA(userAgent, {
      browsers: ["last 2 versions"],
      path: ".",
      allowHigherVersions: true,
    })
  ) {
    browser.uptodate = true;
    browser.matchesLastTwoVersions = true;
  }
  if (
    matchesUA(userAgent, {
      browsers: ["unreleased versions"],
      path: ".",
      allowHigherVersions: true,
    })
  ) {
    console.log("set unreleased from matchesUA");
    browser.uptodate = true;
    browser.unreleased = true;
  }

  if (
    matchesUA(userAgent, {
      browsers: ["Firefox ESR"],
      path: ".",
    })
  ) {
    browser.esr = true;
  }

  // if (bowser.opera && bowser.mobile) {
  //   browser.uptodate = true; // opera mobile doesn't match desktop version
  // }

  // if (bowser.msedge && bowser.android) {
  //   browser.uptodate = true; // since msedge an android has version 41
  // }

  browser.name = result.browser.name;
  browser.internal_name = browser.name;
  browser.real_name = browser.internal_name;
  browser.version = result.browser.version;
  browser.simpleVersion = browser.version?.split(".")[0];

  browser.os = result.os.name;

  // Fix for windows phone internet explorer name
  if (
    browser.name === "Internet Explorer" &&
    result.platform.type === "mobile"
  ) {
    browser.name = "Internet Explorer";
    browser.internal_name = "ie_mob";
    browser.dead = true;
    browser.unreleased = false;
  }

  if (browser.name === "Internet Explorer") {
    browser.internal_name = "ie";
    browser.dead = true;
    browser.unreleased = false;
  }

  if (browser.internal_name === "Chromium") {
    browser.name = "Chromium";
    browser.internal_name = "Chrome";
    browser.real_name = "chromium";
  }

  if (browser.internal_name === "Microsoft Edge") {
    browser.real_name = "edge";
    browser.internal_name = "edge";
  }

  if (browser.internal_name === "Samsung Internet for Android") {
    browser.name = "Samsung Internet";
    browser.internal_name = "samsung";
  }

  const downloadURLs: Record<string, string|null> = {
    firefox: "https://www.mozilla.org/firefox/",
    chrome: "https://www.google.com/chrome/browser/",
    edge: "https://www.microsoft.com/windows/microsoft-edge",
    opera: "https://www.opera.com",
    safari: "https://www.apple.com/safari",
    ie_mob: null,
    "internet explorer": null,
    "microsoft edge": "https://www.microsoft.com/fr-fr/windows/microsoft-edge",
    samsung:
      "https://play.google.com/store/apps/details?id=com.sec.android.app.sbrowser",
  };
  browser.downloadURL = browser.internal_name?.toLowerCase() ? downloadURLs[browser.internal_name?.toLowerCase()] : undefined;

  browser.latest = getLatestVersion(browser.internal_name);

  if (browser.version && browser.latest) {
    console.log("original version", browser.version);
    const saneVersion = browser.version.split(".").slice(0, 2).join(".");

    console.log("saneversion", saneVersion);

    const comparedVersions = compareVersions(
      saneVersion,
      browser.latest?.toString()
    );

    browser.matchesLastVersion = comparedVersions === 0;

    if (comparedVersions > 0) {
      console.log(
        "set unreleased from comparing versions",
        saneVersion,
        browser.latest.toString()
      );
      browser.unreleased = true;
      browser.uptodate = true;
    }

    if (browser.name === "Chromium") {
      browser.uptodate =
        comparedVersions <= 0 || browser.latest.toString() === browser.version;
    }
  }

  if (browser.name === "Android Browser") {
    browser.uptodate = false;
  }

  console.log(browser);

  return browser;
}
