import bowser from "bowser";
import { BSD, LinuxDistributions } from "../constants";

export function osInfo(
  bowserInfo: bowser.Parser.ParsedResult,
  userAgent: string
) {
  if (bowserInfo?.os?.name === "Linux") {
    if (userAgent.match("Fedora")) return LinuxDistributions.FEDORA;
    if (userAgent.match("Ubuntu")) return LinuxDistributions.UBUNTU;
    if (userAgent.match("Debian")) return LinuxDistributions.DEBIAN;
    if (userAgent.match("Arch Linux")) return LinuxDistributions.ARCH_LINUX;
    return LinuxDistributions.LINUX;
  }
  if (userAgent.match("FreeBSD")) return BSD.FREEBSD;

  return bowserInfo?.os?.name;
}
