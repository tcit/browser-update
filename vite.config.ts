import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import { visualizer } from "rollup-plugin-visualizer";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  optimizeDeps: {
    exclude: ["request", "yamlparser"],
  },
  define: {
    "process.env.TEST_PSEUDOMAP": undefined,
  },
  base: "/browser-update/",
  build: {
    rollupOptions: {
      plugins: [visualizer()],
    },
  },
});
